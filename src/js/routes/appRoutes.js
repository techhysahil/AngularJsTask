/*******************************
 File : Router.js
 Author : Sahil Gupta
 Website : http://techhysahil.com/
 *******************************/

angular.module('hcare')
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app', {
            url: "/app",
            templateUrl: 'modules/main/main.html',
            data: {
                authorization: false,
                role : "GUEST"
            }
        })
        .state('app.home', {
            url: "/home",
            templateUrl: "modules/topBanner/topBanner.html",
            data: {
                authorization: false,
                role : "GUEST"
            }
        })
        .state('app.home.list', {
            url: "/list",
            templateUrl: "modules/productList/productList.html",
            data: {
                authorization: false,
                role : "GUEST"
            }
        })
        .state('app.setting', {
            url: "/setting",
            templateUrl: "modules/setting/setting.html",
            data: {
                authorization: true,
                redirectTo: 'app.home.list',
                role : "user"
            }
        })
        .state('app.setting.view', {
            url: "/view",
            templateUrl: "modules/setting/userView.html",
            data: {
                authorization: true,
                redirectTo: 'app.home.list',
                role : "user"
            }
        })
        .state('app.setting.edit', {
            url: "/edit",
            templateUrl: "modules/setting/userEdit.html",
            data: {
                authorization: true,
                redirectTo: 'app.home.list',
                role : "user"
            }
        })
        .state('app.setting.add', {
            url: "/add",
            templateUrl: "modules/setting/userAdd.html",
            data: {
                authorization: true,
                redirectTo: 'app.home.list',
                role : "user"
            }
        })
    $urlRouterProvider.otherwise("app/home/list");
})