/*******************************
 File : SharedService.js
 Author : Sahil Gupta
 Website : http://techhysahil.com/
 *******************************/
angular.module('hcare')
.service('SharedDataService',["$localStorage", function($localStorage){

    /**********************************************
     ****** Error Message Definaition *************
     *********************************************/
    errorText = {
        wrongCrdential : "Wrong credentials, Please enter correct Username and Password"
    }

    /*****************************
     ****** Private Function *****
     ****************************/
    function initlizeModel(){
        if (!$localStorage.userData) {
            $localStorage.userData = [
                {
                    id : 1000,
                    username : "admin",
                    password : "admin",
                    userType : "Admin",
                    email : 'admin@hcare.com',
                    image : 'https://material.angularjs.org/latest/img/list/60.jpeg?0'
                },{
                    id : 1001,
                    username : "manager",
                    password : "manager",
                    userType : "Manager",
                    email : 'manager@hcare.com',
                    image : 'https://material.angularjs.org/latest/img/list/60.jpeg?0'
                },{
                    id : 1003,
                    username : "storekeeper",
                    password : "storekeeper",
                    userType : "StoreKeeper",
                    email : 'storekeeper@hcare.com',
                    image : 'https://material.angularjs.org/latest/img/list/60.jpeg?0'
                }
            ];
        }

        if (!$localStorage.productData) {
            $localStorage.productData =  [
                {
                    Id : 1,
                    Name : "product1",
                    Price : 300,
                    Image : "http://image.made-in-china.com/2f1j00SOytUAsRSbov/Professional-Plastic-Products-of-Gusseted-Bags-Reels-for-Hospital-Use.jpg",
                    isApproved : true
                },{
                    Id : 2,
                    Name : "product2",
                    Price : 1000,
                    Image : "http://www.electricminimassager.com/photo/pl4479678-hospital_product_nursing_care_restraint_straps_medical_wrist_restraints.jpg",
                    isApproved : true
                },{
                    Id : 3,
                    Name : "product3",
                    Price : 4000,
                    Image : "http://web.tradekorea.com/upload_file2/product/932/P00328932/cbe9caa6_9591e742_2864_442a_b1fa_8f9cc2594a44.jpg",
                    isApproved : false
                },{
                    Id : 4,
                    Name : "product4",
                    Price : 9,
                    Image : "http://web.tradekorea.com/upload_file2/product/932/P00328932/cbe9caa6_9591e742_2864_442a_b1fa_8f9cc2594a44.jpg",
                    isApproved : false
                },{
                    Id : 5,
                    Name : "product5",
                    Price : 900,
                    Image : "http://web.tradekorea.com/upload_file2/product/932/P00328932/cbe9caa6_9591e742_2864_442a_b1fa_8f9cc2594a44.jpg",
                    isApproved : true
                },{
                    Id : 6,
                    Name : "product6",
                    Price : 50,
                    Image : "http://web.tradekorea.com/upload_file2/product/932/P00328932/cbe9caa6_9591e742_2864_442a_b1fa_8f9cc2594a44.jpg",
                    isApproved : false
                },{
                    Id : 7,
                    Name : "product7",
                    Price : 90,
                    Image : "http://web.tradekorea.com/upload_file2/product/932/P00328932/cbe9caa6_9591e742_2864_442a_b1fa_8f9cc2594a44.jpg",
                    isApproved : false
                }
            ];

            if (!$localStorage.userType) {
                $localStorage.userType = null;
            }
        }
    }

    function userData(userid){
        var userlist = loadCredentials();
        for (i in userlist){
            if(userlist[i].id === userid){
                return userlist[i];
            }
        }
    }

    function loadCredentials() {
        var savedData = $localStorage.userData;
        if (savedData) {
            return $localStorage.userData;
        }else{
            initlizeModel();
            loadCredentials
        }
    };

    function loadProducts() {
        var savedData = $localStorage.productData;
        if (savedData) {
            return savedData;
        }else{
            initlizeModel();
            loadProducts();
        }
    };

    //Initlize Data and store in in LocalStorage
    initlizeModel();

    /*****************************
     ****** Exposed Function *****
     ****************************/
    this.initlizeModel = initlizeModel;
    this.errorText = errorText;
    this.userData = userData;
    this.loadCredentials = loadCredentials;
    this.loadProducts = loadProducts;
}]);