'use strict';
angular.module('hcare',['ui.router','modulesInitilize','ngStorage','ngAnimate','ngMaterial'])

.run(function($state,$rootScope,$localStorage) {
    //URL Restriction Logic
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

        if (toState.data.authorization === true) {
            console.log("Secured paths");
            if ($localStorage.userType && ($localStorage.userType === "Admin" || $localStorage.userType === "Manager" || $localStorage.userType === "StoreKeeper")) {

            }else{
                $state.go(toState.data.redirectTo);
                event.preventDefault();
            }
        }
    });
})

/*******************************************************
 ****** Disable Debug info For Production **************
 *******************************************************/
.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);;
