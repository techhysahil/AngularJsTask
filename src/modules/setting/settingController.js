angular.module('hcare.SettingCtrl', [])
    .controller('settingController',["$scope", "$rootScope", "$localStorage",  "SharedDataService", "$location", function($scope, $rootScope, $localStorage,  SharedDataService, $location){
        /*****************************
         ****** Flat Construct ******
         ****************************/
        if($localStorage.userType && ($localStorage.userType === "Admin" || $localStorage.userType === "Manager")){
            $scope.isAdduser = true;
        }else{
            $scope.isAdduser = false;
        }
        if($localStorage.userType && $localStorage.userType === "Admin"){
            $scope.isDeleteuser = true;
        }else{
            $scope.isDeleteuser = false;
        }

        if($localStorage.userType && ($localStorage.userType === "Admin"  || $localStorage.userType === "Manager")){
            $scope.isEditUserDetaileuser = true;
        }else{
            $scope.isEditUserDetaileuser = false;
        }
        if($localStorage.userType){
            if($localStorage.userType == "Admin"){
                $scope.addUserRoles = [
                    {role : "Manager"},
                    {role : "StoreKeeper"}
                ];
            }else if($localStorage.userType == "Manager"){
                $scope.addUserRoles = [
                    {role : "StoreKeeper"}
                ];
            }else{
                $scope.addUserRoles = null;
            }

        }
        /*****************************
         ****** Init Dummy Object ****
         ****************************/
        $scope.addUser = {
            username : "",
            password : "",
            userType : "",
            email : '',
            image : ''
        }

        /*******************************************
         ****** Load Data from LocalStorage ********
         ******************************************/
        $scope.userList = SharedDataService.loadCredentials();
        $scope.CurrentUserDetail = SharedDataService.loadCredentials()[0];


        /*****************************
         ****** Exposed Functions ****
         ****************************/
        $scope.openView = function(userid){
            $location.path('/app/setting/view')
            $scope.CurrentUserDetail = SharedDataService.userData(userid);
        }

        $scope.editView = function(userObject){
            $location.path('/app/setting/edit');
            $scope.editDetail = userObject;
        }

        $scope.updateSelectedRole = function(role){
            $scope.currentUserRole = role;
        }

        $scope.addNewUser = function(){
            if($scope.currentUserRole && $scope.addUser.username && $scope.addUser.email && $scope.addUser.password){
                var finalObject = {
                    id : 1000 + Math.floor((Math.random() * 5000) + 1),
                    username : $scope.addUser.username,
                    password : $scope.addUser.password,
                    userType : $scope.currentUserRole,
                    email : $scope.addUser.email,
                    image : $scope.addUser.image || 'https://material.angularjs.org/latest/img/list/60.jpeg?0'
                }
                $localStorage.userData.push(finalObject);
                $scope.addUser = {
                    username : "",
                    password : "",
                    userType : "",
                    email : '',
                    image : ''
                }
            }
        }

        $scope.deleteUser = function(user){
            if(!user.userType === "Admin"){
                console.log("Admin User cann't be deleted");
            }else{
                deleteItem(user.id);
            }

            function deleteItem(uid){
                for(var i=0; i<$localStorage.userData.length; i++){
                    if($localStorage.userData[i].id == uid){
                        $localStorage.userData.splice(i, 1);  //removes 1 element at position i
                        break;
                    }
                }
            }
        }

        $scope.saveUserChanges = function(){
            for(var i=0; i<$localStorage.userData.length; i++){
                if($localStorage.userData[i].id == $scope.editDetail.id){
                    $localStorage.userData[i] = $scope.editDetail;
                    console.log("final data object :" + $localStorage.userData[i]);
                    break;
                }
            }
            $scope.editDetail = {
                username : "",
                password : "",
                userType : "",
                email : '',
                image : ''
            }
            $location.path('/app/setting/view');
        }

    }]);