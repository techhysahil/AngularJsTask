angular.module('hcare.mainCtrl', [])

    .controller('headerCtrl',["$scope", "$state", "$rootScope", "$localStorage", "$mdDialog", "SharedDataService", function($scope, $state, $rootScope, $localStorage, $mdDialog,SharedDataService) {
        /*****************************
         ****** Initlize Flags *******
         ****************************/
        if($localStorage.userType && ($localStorage.userType === "Admin" || $localStorage.userType === "Manager" || $localStorage.userType === "StoreKeeper")){
            $scope.IsUserLogedIn = true;
            $scope.username = $localStorage.username;
        }else{
            $scope.IsUserLogedIn = false;
        }

        /*****************************
         ****** Private Function *****
         ****************************/
        function showDialog($event) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'dialog1.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: $event,
                clickOutsideToClose:true
            })
            function DialogController($scope, $mdDialog) {

                $scope.closeDialog = function() {
                    $mdDialog.hide();
                }

            }
        }
        function logout(){
            $localStorage.userType = null;
            $localStorage.userData = null;
            $localStorage.productData = null;
            SharedDataService.initlizeModel();
            $rootScope.$emit('reinitlizeProductList','');
            $scope.IsUserLogedIn = false;
            $state.go('app.home.list');
        }
        $rootScope.$on('userLoggedIn', function (event, data) {
            $scope.IsUserLogedIn = true;
            $scope.username = data;
            $localStorage.username = data;
        });

        /*****************************
         ****** Public Function ******
         ****************************/
        $scope.showDialog = showDialog;
        $scope.LogoutUser = logout;
    }])

    .controller('AuthController',["$scope", "$rootScope", "$localStorage",  "SharedDataService",function($scope, $rootScope, $localStorage,  SharedDataService){
        /*****************************
         ****** Initlize Flags *******
         ****************************/
        $scope.isLoginFailed = false;
        $scope.user = {
            user : "",
            pass : ""
        };
        $scope.wrongcred = SharedDataService.errorText.wrongCrdential;

        /*****************************
         ****** Load Data *******
         ****************************/
        var AuthData = SharedDataService.loadCredentials();

        /*****************************
         ****** Private Function *****
         ****************************/
        function loginuser(){
            if($scope.user.name && $scope.user.pass){
                for(var i in AuthData){
                    if($scope.user.name === AuthData[i].username && $scope.user.pass === AuthData[i].password){
                        $scope.isLoginFailed = false;
                        $localStorage.userType = AuthData[i].userType;
                        $rootScope.$emit('userLoggedIn',AuthData[i].username);
                        $rootScope.$emit('reinitlizeProductList','');
                        $scope.closeDialog();
                    }else{
                        $scope.isLoginFailed = true;
                    }
                }
            }else{
                $scope.isLoginFailed = true;
            }
        }

        /*****************************
         ****** Public Function ******
         ****************************/
        $scope.loginUser = loginuser;
    }]);