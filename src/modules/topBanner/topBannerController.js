angular.module('hcare.TopBannercontrollers', [])
    .controller('TopBannerCtrl',["$scope", "$state", "$location", "$localStorage", "$anchorScroll", function($scope, $state, $location, $localStorage, $anchorScroll) {
        $scope.scrollTo = function(id) {
            $location.hash(id);
            $anchorScroll();
        }
    }]);