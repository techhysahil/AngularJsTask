angular.module('hcare.ProdoctListCtrl', [])

    .controller('ProdoctListCtrl',["$scope", "$state", "$mdDialog", "$location", "$window", "$rootScope", "$anchorScroll", "$localStorage", function($scope,$state,$mdDialog, $location,$window, $rootScope, $anchorScroll, $localStorage) {
        function init(){
            if($localStorage.userType && $localStorage.userType === "Admin"){
                $scope.isDeleteItem = true;
            }else{
                $scope.isDeleteItem = false;
            }

            if($localStorage.userType && $localStorage.userType === "Manager"){
                $scope.isApproveItem = true;
            }else{
                $scope.isApproveItem = false;
            }

            if($localStorage.userType && ($localStorage.userType === "Admin" || $localStorage.userType === "Manager" || $localStorage.userType === "StoreKeeper")){
                $scope.isAddtocart = false;
            }else{
                $scope.isAddtocart = true;
            }

            if($localStorage.userType && $localStorage.userType === "StoreKeeper"){
                $scope.isAddProduct = true;
            }else{
                $scope.isAddProduct = false;
            }
        }
        init();
        $scope.ProductList = $localStorage.productData;
        $scope.userType = $localStorage.userType;

        $scope.deleteProductItem = function(uid){
            for(var i=0; i<$localStorage.productData.length; i++){
                if($localStorage.productData[i].Id == uid){
                    $localStorage.productData.splice(i, 1);  //removes 1 element at position i
                    break;
                }
            }

        }
        $scope.approveProductItem = function(uid){
            for(var i=0; i<$localStorage.productData.length; i++){
                if($localStorage.productData[i].Id == uid){
                    $localStorage.productData[i].isApproved = true;
                    break;
                }
            }
        }
        $scope.openAddProductDialog = function($event){
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'addProduct.html',
                parent: angular.element(document.body),
                targetEvent: $event,
                clickOutsideToClose:true
            })
            function DialogController($scope, $mdDialog) {

                $scope.closeProductDialog = function() {
                    $mdDialog.hide();
                }

            }
        }
        $rootScope.$on('reinitlizeProductList', function (event, data) {
            $state.reload();
            //$scope.$apply();
        });
    }])
    .controller('addProductController',["$scope","$state","$mdDialog", "$location", "$window", "$rootScope", "$anchorScroll", "$localStorage", function($scope,$state,$mdDialog, $location,$window, $rootScope, $anchorScroll, $localStorage) {
        $scope.addProduct = {
            Name : "",
            Price : "",
            Image : "http://web.tradekorea.com/upload_file2/product/932/P00328932/cbe9caa6_9591e742_2864_442a_b1fa_8f9cc2594a44.jpg",
            isApproved : true
        }
        $scope.addProductItem = function(){
            if($scope.addProduct.Name && $scope.addProduct.Price){
                var finalObject = {
                    id : 1000 + Math.floor((Math.random() * 5000) + 1),
                    Name : $scope.addProduct.Name,
                    Price : $scope.addProduct.Price,
                    Image : $scope.addProduct.Image || "http://web.tradekorea.com/upload_file2/product/932/P00328932/cbe9caa6_9591e742_2864_442a_b1fa_8f9cc2594a44.jpg",
                    isApproved : false
                }
                $localStorage.productData.push(finalObject);
                console.log($localStorage.productData);
                $scope.closeProductDialog();
            }
        }
    }]);